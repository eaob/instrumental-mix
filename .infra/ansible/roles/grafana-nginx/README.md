***Install nginx and create some Vhosts from template***

This role install nginx and create some Vhosts from template on RHEL/CentOS/Fedora systems.


***Example Playbook***

This is an example playbook:
```
- hosts: servers
  roles:
     - { role: install-nginx, when: ansible_os_family == "RedHat" }
```


***Role Variables***

private_key - key to hosts in inventory.

pages - list of pages in menu in index.html generated from template.



***Author Information***


Authored by Evgeniy Oboyanskiy
