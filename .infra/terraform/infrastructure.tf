resource "digitalocean_vpc" "devops" {
  name     = "final-project-network"
  region   = "ams3"
  ip_range = "10.100.100.0/24"
}

resource "digitalocean_droplet" "devops" {
  image  = "ubuntu-18-04-x64"
  name = "final-09"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  vpc_uuid = digitalocean_vpc.devops.id
  ssh_keys = [digitalocean_ssh_key.eaob.id, data.digitalocean_ssh_key.rebrain.id ]
  tags = [digitalocean_tag.droplet.id]
}

resource "digitalocean_droplet" "efk" {
  image  = "ubuntu-18-04-x64"
  name = "efk"
  region = "ams3"
  size   = "s-1vcpu-2gb"
  vpc_uuid = digitalocean_vpc.devops.id
  ssh_keys = [digitalocean_ssh_key.eaob.id, digitalocean_ssh_key.windows.id, data.digitalocean_ssh_key.rebrain.id ]
  tags = [digitalocean_tag.droplet.id]
}

resource "digitalocean_droplet" "monitoring" {
  image  = "ubuntu-18-04-x64"
  name = "monitoring-09"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  vpc_uuid = digitalocean_vpc.devops.id
  ssh_keys = [digitalocean_ssh_key.eaob.id, digitalocean_ssh_key.windows.id, data.digitalocean_ssh_key.rebrain.id ]
  tags = [digitalocean_tag.droplet.id]
}

resource "aws_route53_record" "devops_dns" {
  zone_id = data.aws_route53_zone.devops.zone_id
  name    = "${digitalocean_droplet.devops.name}"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_droplet.devops.ipv4_address}"]
}

resource "aws_route53_record" "systemd_dns" {
  zone_id = data.aws_route53_zone.devops.zone_id
  name    = "systemd-09"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_droplet.devops.ipv4_address}"]
}

resource "aws_route53_record" "monitoring_dns" {
  zone_id = data.aws_route53_zone.devops.zone_id
  name    = "${digitalocean_droplet.monitoring.name}"
  type    = "A"
  ttl     = "300"
  records = ["${digitalocean_droplet.monitoring.ipv4_address}"]
}

resource "null_resource" "monitoring" {
  depends_on = [
  aws_route53_record.monitoring_dns,
  digitalocean_droplet.efk,
  ]
  provisioner "remote-exec" {
  inline = ["echo 'ok'"]
  connection {
    host        = digitalocean_droplet.monitoring.ipv4_address
    type        = "ssh"
    user        = "root"
    private_key = file("/home/centos/.ssh/id_rsa_DO")
    }
  }
  provisioner "local-exec" {
  command = "ansible-playbook -i '${digitalocean_droplet.monitoring.ipv4_address},' --key-file '/home/centos/.ssh/id_rsa_DO' ../ansible/prometheus.yml"
  }
  provisioner "local-exec" {
  command = "ansible-playbook -i '${digitalocean_droplet.monitoring.ipv4_address},' --key-file '/home/centos/.ssh/id_rsa_DO' ../ansible/grafana.yml --extra-vars 'elastic_ip=${digitalocean_droplet.efk.ipv4_address_private}'"
  }
}

resource "null_resource" "devops" {
  provisioner "remote-exec" {
    inline = ["echo 'ok'"]
    connection {
      host        = digitalocean_droplet.devops.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = file("/home/centos/.ssh/id_rsa_DO")
    }
  }
  depends_on = [
  aws_route53_record.devops_dns,
  aws_route53_record.systemd_dns,
  ]
  provisioner "local-exec" {
  command = "ansible-playbook -i '${digitalocean_droplet.devops.ipv4_address},' --key-file '/home/centos/.ssh/id_rsa_DO' ../ansible/october.yml --extra-vars 'efk_ip=${digitalocean_droplet.efk.ipv4_address_private} systemd_name=${aws_route53_record.systemd_dns.name}'"
  }
}

resource "null_resource" "efk" {
  provisioner "remote-exec" {
    inline = ["echo 'ok'"]
    connection {
      host        = digitalocean_droplet.efk.ipv4_address  
      type        = "ssh"
      user        = "root"
      private_key = file("/home/centos/.ssh/id_rsa_DO")
    }
  }
  depends_on = [
  digitalocean_droplet.efk,
  ]
  provisioner "local-exec" {
  command = "ansible-playbook -i '${digitalocean_droplet.efk.ipv4_address},' --key-file '/home/centos/.ssh/id_rsa_DO' ../ansible/efk.yml"
  }
}

data "digitalocean_ssh_key" "rebrain" {
  name = "REBRAIN.SSH.PUB.KEY"
}

resource "digitalocean_ssh_key" "eaob" {
  name       = "eaob"
  public_key = file("/home/centos/.ssh/id_rsa_DO.pub")
}

resource "digitalocean_ssh_key" "windows" {
  name       = "eaob"
  public_key = file("/home/centos/.ssh/windows.pub")
}

resource "digitalocean_tag" "droplet" {
  name = "eaob"
}

data "aws_route53_zone" "devops" {
  name = var.dns_zone
}

output "app_address" {
  value = digitalocean_droplet.devops.ipv4_address
}

output "efk_address" {
  value = digitalocean_droplet.efk.ipv4_address
}

output "monitoring_address" {
  value = digitalocean_droplet.monitoring.ipv4_address
}


