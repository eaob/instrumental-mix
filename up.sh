#!/bin/bash
cp ../DEVOPS/.infra/terraform/terraform.tfvars .infra/terraform/terraform.tfvars
cd .infra/terraform
terraform init
terraform apply -auto-approve
